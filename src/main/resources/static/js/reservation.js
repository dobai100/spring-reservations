/**
 * Script to handle placing reservations.
 */

"use strict";

(function() {
  
  function collectFormInput() {
    
    var reservationFormValues = {};
    $.each($('#reservationForm').serializeArray(), function(i, field) {
      reservationFormValues[field.name] = field.value;
    });
    
    return reservationFormValues;
  }
    

$(document).ready(function() {

  $("#submitReservation").click(function(e) {     
    e.preventDefault();           

    $('.message').hide();
    
    var reservationRequest = collectFormInput();
    
    // TODO: form input validation
      
    function isValidDate(dateString) {
    	
    	  var regEx = /^\d{4}-\d{2}-\d{2}$/;
    	  
    	  if(!dateString.match(regEx)) {
    		  return false;  // Invalid format
    	  }
    	  
    	  var d = new Date(dateString);
    	  
    	  if(!d.getTime()) {
    		  return false; // Invalid date (or this could be epoch)
    	  }
    	  return d.toISOString().slice(0,10) === dateString;
    }
    
    var reservationUrl = 
      '/api/reservations/' + encodeURIComponent(reservationRequest.resourceId) 
      + '/from-' + encodeURIComponent(reservationRequest.fromDate) 
      + '/to-' + encodeURIComponent(reservationRequest.toDate)
      + '/?owner=' + encodeURIComponent(reservationRequest.owner);
    
    if (!$.isNumeric(reservationRequest.resourceId)){
    	$('#validation-error').show();
    }
    
    if (!isValidDate(reservationRequest.fromDate)){
    	$('#date-error').show();
    }
    
    if (!isValidDate(reservationRequest.toDate)){
    	$('#date-error').show();
    }

    if(Date.parse(reservationRequest.fromDate) > Date.parse(reservationRequest.toDate)){
    	$('#range-error').show();
    }
    
    $.ajax({
      url : reservationUrl,                    
      method: 'POST',                           
      async : true,                          
      cache : false,                         
      timeout : 5000,                     

      data : {},                               

      success : function(data, statusText, response) {          
        var reservationId = response.getResponseHeader('reservation-id');
        $('#reservationId').text(reservationId);
        $('#reservation-successful-message').show();
        document.getElementById("reservationForm").reset();
      },
                                                
      error : function(XMLHttpRequest, textStatus, errorThrown) {   
        console.log("reservation request failed ... HTTP status code: " + XMLHttpRequest.status + ' message ' + XMLHttpRequest.responseText);
        
        var errorCodeToHtmlIdMap = {400 : '#validation-error', 405 : '#validation-error', 409 : '#conflict-error' , 500: '#system-error'};
        var id = errorCodeToHtmlIdMap[XMLHttpRequest.status];
        
        if (!id) {
          id =  errorCodeToHtmlIdMap[500]; 
        }
        
        $(id).fadeIn();
      }
    });
    
  });

  
});


})();